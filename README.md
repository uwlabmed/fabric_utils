# fabric_utils: Utilities that extend Fabric and/or Cuisine to simplify deployment scripts #

## Installation ##

The best way to use fabric_utils is to install using pip in the interpreter that the fab command will use. Command to install using pip is 


```
#!bash

pip install git+ssh://git@bitbucket.org/uwlabmed/fabric_utils.git  
```

*NOTE: the URL is the ssh URL from bitbucket with the ":" in bitbucket.org:uwlabmed replaced by a "/".* You'll need access to the labmed bitbucket account unless/until this repo can be made public.

## Usage ##

Import the utils into your fabfile.py and enjoy the convenience.

## Contents ##

### rsync with sudo ###

The Fabric *contrib.rsync_project()* function provides no way to use sudo on the target host. But sometimes you need to
use sudo, so this utility provides a way to do that. It looks like, and indeed is, a terrible kludge, but it's the
simplest, safest, and most reliable way that I've found to do it. The method of operation is to add a line to the /etc/sudoers file that permits the user to run *sudo rsync* without providing a password. This permission is enabled, *sudo rsync* is run, and the permission is disabled. Needless to say, only a user with *sudo* permission will have the ability to alter the /etc/sudoers file.

The arguments for the function are explained by its embedded documentation, but basically you import it with



```
#!python

from fabric_utils import rsync
```

and call it as

  
```
#!python

rsync_sudo(target_path, [source path], [target_user], [other args as for rsync_project])

```

The source path defaults to the current working directory and the target user defaults to the current user name.