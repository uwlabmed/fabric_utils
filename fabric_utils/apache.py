from fabric.api import put, sudo
from fabric.contrib.files import exists
import os


class InvalidCertOrKey(Exception):
    pass


class NoSuchFile(InvalidCertOrKey):
    def __str__(self):
        return 'No such file: ' + self.message


class InvalidCertFile(InvalidCertOrKey):
    def __str__(self):
        return 'Invalid cert file: ' + self.message


class InvalidKeyFile(InvalidCertOrKey):
    def __str__(self):
        return 'Invalid key file: ' + self.message


class MissingSnakeoilCert(InvalidCertOrKey):
    def __str__(self):
        return 'Missing snakeoil cert.  Target OS may not be supported.'


def validate_cert(cert, key):
    """
    Validate ssl certificate files ``cert`` and ``key``.
    Raises exceptions derived from InvalidCertOrKey to communicate errors.
    """
    if not os.path.isfile(cert):
        raise NoSuchFile(cert)
    if not os.path.isfile(key):
        raise NoSuchFile(key)
    with open(cert) as f:
        if 'BEGIN CERTIFICATE' not in f.read():
            raise InvalidCertFile(cert)
    with open(key) as f:
        if 'BEGIN PRIVATE KEY' not in f.read():
            raise InvalidKeyFile(key)


def put_cert(cert, key, name):
    """
    Put ssl certificate files ``cert`` and ``key`` on target host in respective
    /etc/ssl subdirectories, using ``name`` as target filename stem.

    If either cert or key are empty strings, snakeoil cert will be used instead.

    Returns tuple(cert_dest, key_dest) of destination paths (on target) of cert
    and key files.
    """
    if cert and key:
        validate_cert(cert, key)

        # put cert files.
        cert_dest = '/etc/ssl/certs/{}.pem'.format(name)
        key_dest = '/etc/ssl/private/{}.key'.format(name)
        put(cert, cert_dest, mode=0644, use_sudo=True)
        put(key, key_dest, mode=0640, use_sudo=True)
        sudo('chown root.root {}'.format(cert_dest))
        sudo('chown root.ssl-cert {}'.format(key_dest))
    else:
        # use snakeoil cert.
        cert_dest = '/etc/ssl/certs/ssl-cert-snakeoil.pem'
        key_dest = '/etc/ssl/private/ssl-cert-snakeoil.key'

        # these files should always be present; they appear to be created
        # when the OS is installed; but check anyway.
        if not exists(cert_dest) or not exists(key_dest, use_sudo=True):
            raise MissingSnakeoilCert()

    return cert_dest, key_dest
