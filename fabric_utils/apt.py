
from fabric.api import sudo, task, settings


class AptGet(object):
    """
    Class encapsulating apt-get command operations targeting remote hosts using fabric.
    """
    @staticmethod
    def install(packages, do_update=True, do_upgrade=False, options=None):
        '''
        Install system packages using apt-get.
        Also, update and (optionally) upgrade package lists before install to ensure
        successful install.
        '''
        options = options if options else ''
        if do_update:
            sudo('apt-get update -q')
        if do_upgrade:
            sudo('apt-get upgrade -qy')

        # make the install REALLY non-interactive.  without this apt-get might throw
        # curses-based prompts at you, which fabric cannot handle.
        DFNI = 'env DEBIAN_FRONTEND=noninteractive'
        sudo('{} apt-get install -qy {} {}'.format(DFNI, options, ' '.join(packages)))

    @staticmethod
    def upgrade():
        '''
        Upgrade system packages using apt-get.
        Also, update package lists before upgrade to ensure successful upgrade,
        and run checkrestart (from debian-goodies package) afterward to inform user
        what processes and services will need to be restarted manually.
        '''
        sudo('apt-get update -q')
        sudo('apt-get upgrade -qy')
        with settings(ok_ret_codes=[0, 127]):
            sudo('checkrestart -v')

    @staticmethod
    def dist_upgrade():
        '''
        Dist-upgrade system packages using apt-get.
        Also, update package lists before upgrade to ensure successful dist-upgrade,
        and run checkrestart (from debian-goodies package) afterward to inform user
        what processes and services will need to be restarted manually.
        '''
        sudo('apt-get update -q')
        sudo('apt-get dist-upgrade -qy')
        with settings(ok_ret_codes=[0, 127]):
            sudo('checkrestart -v')


apt_get = AptGet()


@task
def apt_get_upgrade():
    apt_get.upgrade()


@task
def apt_get_dist_upgrade():
    apt_get.dist_upgrade()
