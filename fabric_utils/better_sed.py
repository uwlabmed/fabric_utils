import pipes
from fabric.api import sudo, run


def better_sed(filename, before, after, **kwargs):
    """
    a replacement for fabric.contrib.files.sed which does a better job of quoting
    arguments and adds a delimiter parameter.  specifically, uses pipes.quote to wrap
    the arguments to the generated sed command.  this enables embedding single quotes
    which otherwise cannot be done.
    """
    executor = sudo if kwargs.get('use_sudo') else run
    backup = kwargs.get('backup', '.bak')
    delimiter = kwargs.get('delimiter', '/')
    assert len(delimiter) == 1

    limit = kwargs.get('limit', '')
    if limit:
        limit = '/{}/ '.format(limit)
    flags = kwargs.get('flags', '')

    backup_arg = pipes.quote(backup)
    pattern_arg = pipes.quote('{0}s{1}{2}{1}{3}{1}{4}g'.format(limit, delimiter,
                                                               before, after, flags))
    fn_arg = pipes.quote(filename)
    executor('sed -i{} -r -e {} {}'.format(backup_arg, pattern_arg, fn_arg))
