
from fabric.contrib.files import append, comment, uncomment, contains
import re


def ensure_line_in_file(filename, line, use_sudo=False):
    """
    Ensure a file contains the given line.  This will be achieved by
    uncommenting the line, or by appending the line to the file.
    """
    # escape slashes in the line as they are control chars in sed (which
    # is called by uncomment).
    line_escape = re.sub('/', '\\/', line)

    # try to uncomment the line.
    uncomment(filename, line_escape, use_sudo=use_sudo)

    # append the line if it does not already exist.
    if not contains(filename, line, exact=True, use_sudo=use_sudo):
        append(filename, line, use_sudo=use_sudo)


def ensure_etc_hosts_entry(ip, host):
    """
    Ensure an entry in /etc/hosts exists for mapping `ip` to `host`.
    This is useful for testing with hosts that have dynamic IPs.
    """
    # comment out lines that would conflict with the given mapping.
    comment('/etc/hosts', '^{}'.format(ip), use_sudo=True)
    comment('/etc/hosts', '^172.*{}'.format(host), use_sudo=True)
    comment('/etc/hosts', '^10.*{}'.format(host), use_sudo=True)

    # then ensure the line.
    ensure_line_in_file('/etc/hosts', '{} {}'.format(ip, host), use_sudo=True)
