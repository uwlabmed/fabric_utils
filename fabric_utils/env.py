
from fabric.api import env


def fab_env_is_set_true(key):
    '''
    Return true if fabric env var ``key`` is set to '1' or [yY].*, otherwise
    return false.
    '''
    value = env.get(key, '0')
    return value == '1' or (isinstance(value, str) and value[0].lower() == 'y')
