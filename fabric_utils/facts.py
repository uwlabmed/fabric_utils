
from fabric.api import run


def memtotal_mb():
    return int(run('free -m | grep ^Mem:').split()[1])


def num_cpus():
    return int(run('cat /proc/cpuinfo | grep ^processor | wc -l'))
