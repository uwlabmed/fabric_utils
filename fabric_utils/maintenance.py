
from __future__ import print_function
from fabric.api import task, settings, sudo, hide
from text import shrink_tabular_whitespace


@task
def reboot():
    """
    Replacement for fabric.api.reboot that continues despite return code -1.
    """
    with settings(ok_ret_codes=[0, -1]):
        sudo('reboot')


@task
def netstat_tupln():
    """
    Wrapper for 'netstat -tupln' command to print all network listening processes.
    """
    with hide('output'):
        result = shrink_tabular_whitespace(sudo('netstat -tupln | tail -n +2'))
    print('\n' + result + '\n')
