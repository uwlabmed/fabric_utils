import base64
import os

MIN_LENGTH = 8


class PasswordTooShort(Exception):
    def __str__(self):
        return 'Password is too short (min length is {})'.format(MIN_LENGTH)


def make_password(length):
    if length < MIN_LENGTH:
        raise PasswordTooShort()
    return base64.b64encode(os.urandom(length))[:length]
