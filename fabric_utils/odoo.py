
from fabric.api import sudo
from fabric.contrib.files import contains, append
from better_sed import better_sed as sed


class Odoo(object):
    """
    Class encapsulating configuration operations on a remote odoo server.
    """
    def __init__(self, port=8069, longpoll_port=8072):
        self.port = port
        self.longpoll_port = longpoll_port
        self.odoo_conf_path = '/etc/odoo/openerp-server.conf'
        self.addons_dir = '/usr/lib/python2.7/dist-packages/openerp/addons'

    def set_odoo_conf(self, key, value, **kwargs):
        """
        Set variable ``key`` in odoo config file to ``value``.
        """
        kwargs['use_sudo'] = True
        key_pattern = '^(; *)?{} = .*$'.format(key)
        if contains(self.odoo_conf_path, key_pattern, escape=False, use_sudo=True):
            sed(self.odoo_conf_path, key_pattern, '{} = {}'.format(key, value),
                delimiter='|', **kwargs)
        else:
            append(self.odoo_conf_path, '{} = {}'.format(key, value), **kwargs)

    def restart(self):
        sudo('service odoo restart')
