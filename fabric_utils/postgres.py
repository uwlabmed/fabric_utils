
from fabric.api import sudo, settings, task
from better_sed import better_sed as sed
from ensure import ensure_line_in_file
import re


class PostgresException(Exception):
    pass


class BadPassword(PostgresException):
    def __str__(self):
        return 'Bad password: contains apostrophe'


class BadName(PostgresException):
    def __str__(self):
        return 'Bad name: must be sequence of alphanumeric chars and underscore'


class BadAddress(PostgresException):
    def __str__(self):
        return 'Bad address: must include CIDR mask or start with "same"'


class BadDatabaseAction(PostgresException):
    def __str__(self):
        return 'Bad database action: must be "create" or "drop"'


def validate_password(pw):
    if '\'' in pw:
        raise BadPassword()


def validate_name(name):
    if not re.match('^[0-9A-Za-z_]+$', name):
        raise BadName()


def validate_address(addr):
    if not ('/' in addr or addr.startswith('same')):
        raise BadAddress()


def validate_database_action(action):
    if action not in ['create', 'drop']:
        raise BadDatabaseAction()


class Postgres(object):
    """
    Class encapsulating configuration operations on a remote postgresql database server.
    """
    def __init__(self, version=None, admin_user=None, home_dir=None):
        self.version = version if version else '9.5'
        self.admin_user = admin_user if admin_user else 'postgres'
        self.home_dir = home_dir if home_dir else '/var/lib/postgresql'

    @property
    def pg_config_dir(self):
        return '/etc/postgresql/{}/main'.format(self.version)

    @property
    def pg_bin_dir(self):
        return '/usr/lib/postgresql/{}/bin'.format(self.version)

    @property
    def pg_data_dir(self):
        return '{}/{}/main'.format(self.home_dir, self.version)

    @property
    def archive_log_dir(self):
        return '{}/{}/archive_logs'.format(self.home_dir, self.version)

    @property
    def pg_conf_path(self):
        return '{}/postgresql.conf'.format(self.pg_config_dir)

    @property
    def pg_hba_conf_path(self):
        return '{}/pg_hba.conf'.format(self.pg_config_dir)

    @property
    def pg_ctl_path(self):
        return '{}/pg_ctl'.format(self.pg_bin_dir)

    def set_pg_conf(self, key, value, **kwargs):
        """
        Set variable ``key`` in postgresql.conf to ``value``.
        """
        kwargs['use_sudo'] = True
        sed(self.pg_conf_path,
            '^#?{} = .*$'.format(key), '{} = {}'.format(key, value), **kwargs)

    def add_or_update_user(self, user, pw):
        validate_name(user)
        validate_password(pw)
        with settings(sudo_user=self.admin_user):
            with settings(ok_ret_codes=[0, 1]):
                sudo('psql -c "create role {}"'.format(user))
            sudo('psql -c "alter role {} with login password \'{}\'"'.format(user, pw))

    def database_action(self, db, action):
        validate_name(db)
        validate_database_action(action)
        with settings(sudo_user=self.admin_user):
            with settings(ok_ret_codes=[0, 1]):
                sudo('psql -c "{} database {}"'.format(action, db))

    def create_database(self, db):
        self.database_action(db, 'create')

    def drop_database(self, db):
        self.database_action(db, 'drop')

    def add_local_hba_entry(self, user, db, method=None, options=None):
        method = method if method else 'md5'
        options = options if options else ''
        line_fmt = 'local    {}    {}    {}    {}'
        ensure_line_in_file(self.pg_hba_conf_path,
                            line_fmt.format(db, user, method, options),
                            use_sudo=True)

    def add_hba_entry(self, user, db, address, conntype=None, method=None, options=None):
        conntype = conntype if conntype else 'hostssl'
        method = method if method else 'md5'
        options = options if options else ''
        validate_address(address)
        line_fmt = '{}    {}    {}    {}    {}    {}'
        ensure_line_in_file(self.pg_hba_conf_path,
                            line_fmt.format(conntype, db, user, address, method, options),
                            use_sudo=True)

    def restart(self):
        sudo('service postgresql restart')

    def hup(self):
        """
        Cause server to reload config without restarting.
        """
        with settings(sudo_user=self.admin_user):
            sudo('env PGDATA={} {} reload'.format(self.pg_data_dir, self.pg_ctl_path))


@task
def postgres_hup():
    Postgres().hup()
