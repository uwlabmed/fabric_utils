from fabric.api import run, env, settings
from fabric.contrib.project import rsync_project
from fabric.contrib.files import comment, uncomment, append
from itertools import chain
from os.path import normpath
import re


env.use_ssh_config = True
env.use_sudo = True

SUDOERS = '/etc/sudoers'

def contents_of(path):
    ''' Convenience function to make explicit that rsync should copy the contents of a directory.'''
    return normpath(path) + '/'


def rsync_sudo(remote, local_dir=None, user=None, group=None, exclude=(), delete=False, extra_opts='',
               ssh_opts='',capture=False, upload=True, default_opts='-pthrvz'):
    '''
    Copy files to remote host using rsync with sudo. Adds optional args to specifiy user and group to be used on remote
    host. All other args are the same as those for Fabric's rsync_project() function. NOTE: If you get the dreaded "is
    your terminal clean?" message then comment out the "requiretty" and "!visiblepw" default options in /etc/sudoers.
    When ssh uses a pseudo-terminal the rsync binary protocol generally gets corrupted.

    :param user identity to assume on remote host for rsync.
    :param group identity to assume on remote host for rsync.
    '''

    install_user = run('whoami')
    owner = user or env.sudo_user or install_user
    own_group = group or run('id -gn %s' % owner)
    enable_rsync_sudo = r'%s ALL= (%s) NOPASSWD:/usr/bin/rsync  #FABRIC' % (install_user, owner)

    try:
        with(settings(sudo_user='root')):
            append(SUDOERS, '#' + enable_rsync_sudo, use_sudo=True)
            uncomment(SUDOERS, re.escape(enable_rsync_sudo), use_sudo=True)
        rsync_cmd = 'sudo -u %s -g %s -n /usr/bin/rsync' % (owner, own_group)
        rsync_project(remote, local_dir=local_dir,
                      extra_opts=' '.join((extra_opts, '--rsync-path="%s"' % rsync_cmd)),
                      exclude=tuple(chain(('*.pyc','.git*', '.idea'), exclude)), delete=delete,
                      ssh_opts=' '.join(('-T', ssh_opts)), capture=capture, upload=upload,
                      default_opts=default_opts)

    finally:
        with(settings(sudo_user='root')):
            comment(SUDOERS, '^' + re.sub('([%s])' % re.escape("()"), r'\\\1', enable_rsync_sudo), use_sudo=True)
