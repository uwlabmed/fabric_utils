
from cStringIO import StringIO


def shrink_line(line, del_indices):
    """
    Return ``line`` with characters removed at indices given by iterable ``del_indices``.
    """
    output = StringIO()
    for idx, char in enumerate(line):
        if idx not in del_indices:
            output.write(char)

    result = output.getvalue()
    output.close()
    return result


def shrink_tabular_whitespace(text):
    """
    Shrink columns of whitespace in input text from areas (multi-column spans) to lines
    (single-column spans).
    """
    lines = [line for line in text.splitlines()]
    min_len = min([len(line) for line in lines])
    ws_columns = []
    has_single = 0  # state variable to preserve single-column spans.
    for idx in range(min_len):
        is_space = True
        for line in lines:
            if line[idx] != ' ':
                is_space = False
                break

        if is_space:
            if has_single:
                ws_columns.append(idx)
            else:
                has_single = True
        else:
            has_single = False

    return '\n'.join([shrink_line(line, ws_columns) for line in lines])
