from distutils.core import setup
from setuptools import find_packages


setup(
    name='fabric_utils',
    version='0.1',
    packages=find_packages(),
    url='',
    license='',
    author='elgow',
    author_email='elgow@uw.edu',
    description='Utility extensions for the Fabric deployment tool'
)
