
import os
import unittest

from OpenSSL import crypto
from tempfile import NamedTemporaryFile

from fabric_utils.mkpass import make_password
from fabric_utils.apache import validate_cert, NoSuchFile, InvalidCertFile, InvalidKeyFile

snakeoil_cert = '/etc/ssl/certs/ssl-cert-snakeoil.pem'


def make_key():
    file_ = NamedTemporaryFile(delete=False)
    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 1024)
    file_.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))
    file_.close()
    return file_.name


class TestValidateCert(unittest.TestCase):
    def test_no_such_file(self):
        filename = make_password(80)
        with self.assertRaises(NoSuchFile):
            validate_cert(filename, filename)

    def test_invalid_cert(self):
        filename = make_key()
        with self.assertRaises(InvalidCertFile):
            validate_cert(filename, filename)
        os.remove(filename)

    def test_invalid_key(self):
        with self.assertRaises(InvalidKeyFile):
            validate_cert(snakeoil_cert, snakeoil_cert)

    def test_good(self):
        filename = make_key()
        validate_cert(snakeoil_cert, filename)
        os.remove(filename)
