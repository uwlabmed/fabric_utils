
import unittest

from fabric_utils.mkpass import make_password


class TestMkpass(unittest.TestCase):
    def test_password_length(self):
        for length in range(8, 200):
            assert len(make_password(length)) == length

    def test_passwords_always_differ(self):
        pws = [make_password(10) for x in range(500)]
        while len(pws):
            pw = pws.pop()
            for y in pws:
                assert pw != y, 'repeated password is: {}'.format(y)
