
import unittest

from fabric_utils.mkpass import make_password
from fabric_utils.postgres import BadPassword, BadName, validate_password, validate_name
from fabric_utils.postgres import BadAddress, validate_address
from fabric_utils.postgres import BadDatabaseAction, validate_database_action


class TestPostgres(unittest.TestCase):
    def test_validate_password_bad(self):
        with self.assertRaises(BadPassword):
            validate_password('\'; evil')

    def test_validate_password_good(self):
        validate_password(make_password(10))

    def test_validate_name_bad_1(self):
        with self.assertRaises(BadName):
            validate_name('; evil')

    def test_validate_name_bad_2(self):
        with self.assertRaises(BadName):
            validate_name('+')

    def test_validate_name_good(self):
        validate_name('x')

    def test_validate_address_bad(self):
        with self.assertRaises(BadAddress):
            validate_address('10.10.10.100')

    def test_validate_address_good_1(self):
        validate_address('10.10.10.100/32')

    def test_validate_address_good_2(self):
        validate_address('samehost')

    def test_validate_database_action_bad(self):
        with self.assertRaises(BadDatabaseAction):
            validate_database_action('wiggle')
